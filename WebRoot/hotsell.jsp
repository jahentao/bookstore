<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div class="hotsell">
		<div class="hotsell_box">
			<div class="hotsell_head">
				<h4>热销推荐</h4>
			</div>
			<ul class="hotsell_list">
				<s:iterator value="#request['hotsellBooksDetails']" id="detail">
					<li class="hotsell_item">
						<a class="rec-item-inner">
							<img class="rec-item-img" src="/bookstore/picture/<s:property value="#detail.id.picture"/>">
						
							<h3 class="rec-item-title"><s:property value="#detail.id.bookname"/></h3>
								<div class="rec-item-summary">
									<div class="rec-item-sum">
										<span>总销量:</span> <strong><s:property value="#detail.id.quantities"/></strong>
									</div>
									<div class="rec-item-price">
										<span>&yen;</span> <strong><s:property value="#detail.id.price"/></strong>
									</div>
									<button href="javascript:void(0)" class="rec-cart j_AddCart"></button>
								</div>
						</a>
					</li>
				</s:iterator>
			</ul>
		</div>
	</div>
</body>
</html>