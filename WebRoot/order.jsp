<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>网上购书系统</title>
<link href="css/bookstore.css" rel="stylesheet" type="text/css">
</head>
<body>
	<jsp:include page="head.jsp"></jsp:include>
	<div class=content>
		<div class=left>
			<div class=list_box>
				<div class=list_bk>
					<s:action name="browseCatalog" executeResult="true" />
				</div>
			</div>
		</div>
		<div class=right>
			<div class=right_box>
				<s:set name="orders" value="#request.myorders" />
				<s:if test="#orders.size != 0">
					<h3>
						<font color="blue">您的订单</font>
					</h3>

					<s:iterator value="#orders" id="order">
						<div class="info_bk1">
							订单号：
							<s:property value="#order.orderid" />
							<table width="600" border="0">
									<s:iterator value="#order.orderitems" id="item">
										<tr align="left">
											<td width="50">书名：</td>
											<td width="100"><s:property value="#item.book.bookname" /></td>
											<td width="50">价格：</td>
											<td width="50"><s:property value="#item.book.price" /></td>
											<td width="50">数量：</td>
											<td width="50"><s:property value="#item.quantity" /></td>
										</tr>


									</s:iterator>
							
							</table>
						</div>
					</s:iterator>

					<%-- 消费金额:<s:property value="#session.cart.totalPrice" />元&nbsp;&nbsp;&nbsp; --%>
					<!-- <a href="checkout.action"><img
						src="/bookstore/picture/count.gif" /></a> -->
				</s:if>
				<s:else>
					对不起,您还没有选购任何书籍!
				</s:else>
			</div>
		</div>
	</div>
	<jsp:include page="foot.jsp"></jsp:include>
</body>
</html>