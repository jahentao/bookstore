CREATE VIEW v_book_quantities
AS SELECT
         COUNT(quantity) quantities,
         bookid
       FROM `orderitem`
       GROUP BY bookid;
