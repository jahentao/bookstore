﻿/*
  函数 f_getCatalogSellAmount
  输入参数：p_catalogid 书籍类别id
  返回参数：v_sell_amount 该类别的销售量
*/
DROP FUNCTION IF EXISTS f_getCatalogSellAmount;

delimiter //
CREATE FUNCTION f_getCatalogSellAmount (p_catalogid int UNSIGNED)
RETURNS int
READS SQL DATA
BEGIN
  DECLARE v_done int DEFAULT FALSE;
  DECLARE v_bid int;
  DECLARE v_q int;
  DECLARE v_sell_amount int;
  -- 查询catalogid类别下的bookid
  DECLARE bookid_cursor CURSOR FOR
  SELECT
    bookid
  FROM `book`
  WHERE `book`.catalogid = p_catalogid;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = TRUE;

  SET v_sell_amount = 0;
  -- 统计该类别下书籍的销量
  OPEN bookid_cursor;
read_loop:
  LOOP
    FETCH NEXT FROM bookid_cursor INTO v_bid;
    IF v_done THEN
      LEAVE read_loop;
    END IF;
    -- 嵌套查询
    SELECT
      quantities INTO v_q
    FROM (SELECT
        COUNT(quantity) quantities,
        bookid
      FROM `orderitem`
      GROUP BY bookid) AS t
    WHERE t.bookid = v_bid;
    SET v_sell_amount = v_sell_amount + v_q;
  END LOOP;
  CLOSE bookid_cursor;
  RETURN v_sell_amount;
END//

delimiter ;