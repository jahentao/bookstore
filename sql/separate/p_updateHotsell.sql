﻿/**
存储过程 p_updateHotsell 定时更新热销商品表
*/
DROP PROCEDURE IF EXISTS p_updateHotsell;

delimiter //
CREATE PROCEDURE p_updateHotsell ()
COMMENT '更新热销商品表'
BEGIN
  -- 清除表中数据
  DELETE
    FROM `hotsell`
  WHERE 1 = 1;

  -- 把视图表里的数据按销量排序后，插入表中
  INSERT INTO hotsell (bookid, catalogid, salevolume)
    SELECT
      bookid,
      catalogid,
      quantities
    FROM v_booksaledetails
    ORDER BY quantities DESC;
END//

delimiter ;