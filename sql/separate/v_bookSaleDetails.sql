﻿/*
  创建视图 v_book_sale_detail
  查看书籍销售量
*/
DROP VIEW IF EXISTS v_bookSaleDetails;
CREATE VIEW v_bookSaleDetails
AS
SELECT DISTINCT
  b.bookid,
  b.bookname,
  b.price,
  b.picture,
  c.catalogid,
  c.catalogname,
  d.quantities
FROM book b,
     catalog c,
     v_book_quantities d
WHERE b.catalogid = c.catalogid AND d.bookid=b.bookid;
