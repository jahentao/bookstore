﻿/*
  创建数据库定时任务
  e_updateHotsell
*/
CREATE EVENT `e_updateHotsell` ON SCHEDULE EVERY 1 MINUTE STARTS CURRENT_TIMESTAMP
ON COMPLETION NOT PRESERVE ENABLE 
DO CALL p_updateHotsell();
-- 查看当前库下面所有的 job
show events;
-- 查看job是否开启了自动执行
show global variables like '%event_scheduler%';
-- 开启job自动执行
set global event_scheduler = on;  