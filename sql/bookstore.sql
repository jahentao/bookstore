/*
MySQL Data Transfer
Source Host: localhost
Source Database: bookstore
Target Host: localhost
Target Database: bookstore
Date: 2017/4/25 16:28:28
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for book
-- ----------------------------
CREATE TABLE `book` (
  `bookid` int(11) NOT NULL AUTO_INCREMENT,
  `bookname` varchar(20) NOT NULL,
  `price` int(11) NOT NULL,
  `picture` varchar(30) DEFAULT NULL,
  `catalogid` int(11) NOT NULL,
  PRIMARY KEY (`bookid`),
  KEY `book_catalogid_catalog` (`catalogid`),
  CONSTRAINT `book_catalogid_catalog` FOREIGN KEY (`catalogid`) REFERENCES `catalog` (`catalogid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for catalog
-- ----------------------------
CREATE TABLE `catalog` (
  `catalogid` int(11) NOT NULL AUTO_INCREMENT,
  `catalogname` varchar(20) NOT NULL,
  PRIMARY KEY (`catalogid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orderitem
-- ----------------------------
CREATE TABLE `orderitem` (
  `orderitemid` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  PRIMARY KEY (`orderitemid`),
  KEY `orderitem_orderid_orders` (`orderid`),
  KEY `orseritem_bookid_book` (`bookid`),
  CONSTRAINT `orderitem_orderid_orders` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orseritem_bookid_book` FOREIGN KEY (`bookid`) REFERENCES `book` (`bookid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `orderdate` datetime NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records
-- ----------------------------
INSERT INTO `book` VALUES ('1', 'ASP.NET教程', '50', 'ASP.NET4.0.jpg', '1');
INSERT INTO `book` VALUES ('2', 'C#实用教程', '49', 'C.jpg', '1');
INSERT INTO `book` VALUES ('3', 'SQLSERVIC实用教程', '59', 'SQL.jpg', '1');
INSERT INTO `book` VALUES ('4', 'MySql实用教程', '53', 'MySQL.jpg', '1');
INSERT INTO `book` VALUES ('5', 'PoweBuider', '53', 'PB.jpg', '1');
INSERT INTO `catalog` VALUES ('1', '编程书籍');
INSERT INTO `orderitem` VALUES ('1', '5', '1', '2');
INSERT INTO `orderitem` VALUES ('2', '4', '2', '1');
INSERT INTO `orderitem` VALUES ('3', '1', '3', '3');
INSERT INTO `orders` VALUES ('1', '2017-04-25 16:22:58', '1');
INSERT INTO `orders` VALUES ('2', '2017-04-24 16:25:33', '2');
INSERT INTO `orders` VALUES ('3', '2017-04-19 16:25:47', '3');
INSERT INTO `users` VALUES ('1', '997', 'tiamo', '男', '20');
INSERT INTO `users` VALUES ('2', 'rlj', '123456', '男', '20');
INSERT INTO `users` VALUES ('3', 'jahentao', 'jahentao', '男', '21');
