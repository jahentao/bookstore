-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: rm-wz9ipfscd2c9u79jno.mysql.rds.aliyuncs.com    Database: bookstore
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='cd588991-3462-11e7-aa2f-00163e02afe0:1-14945';

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `bookid` int(11) NOT NULL AUTO_INCREMENT,
  `bookname` varchar(20) NOT NULL,
  `price` int(11) NOT NULL,
  `picture` varchar(30) DEFAULT NULL,
  `catalogid` int(11) NOT NULL,
  PRIMARY KEY (`bookid`),
  KEY `book_catalogid_catalog` (`catalogid`),
  CONSTRAINT `book_catalogid_catalog` FOREIGN KEY (`catalogid`) REFERENCES `catalog` (`catalogid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'ASP.NET教程',50,'ASP.NET4.0.jpg',1),(2,'C#实用教程',49,'C.jpg',1),(3,'SQLSERVIC实用教程',59,'SQL.jpg',1),(4,'MySql实用教程',53,'MySQL.jpg',1),(5,'PoweBuider',53,'PB.jpg',1);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `catalogid` int(11) NOT NULL AUTO_INCREMENT,
  `catalogname` varchar(20) NOT NULL,
  PRIMARY KEY (`catalogid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'编程书籍');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotsell`
--

DROP TABLE IF EXISTS `hotsell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotsell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookid` int(11) NOT NULL,
  `catalogid` int(11) NOT NULL,
  `salevolume` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_BOOKID` (`bookid`),
  KEY `FK_CATALOGID` (`catalogid`),
  CONSTRAINT `FK_BOOKID` FOREIGN KEY (`bookid`) REFERENCES `book` (`bookid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGID` FOREIGN KEY (`catalogid`) REFERENCES `catalog` (`catalogid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2995 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotsell`
--

LOCK TABLES `hotsell` WRITE;
/*!40000 ALTER TABLE `hotsell` DISABLE KEYS */;
INSERT INTO `hotsell` VALUES (2992,1,1,2),(2993,2,1,2),(2994,3,1,1);
/*!40000 ALTER TABLE `hotsell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderitem`
--

DROP TABLE IF EXISTS `orderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderitem` (
  `orderitemid` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  PRIMARY KEY (`orderitemid`),
  KEY `orderitem_orderid_orders` (`orderid`),
  KEY `orseritem_bookid_book` (`bookid`),
  CONSTRAINT `orderitem_orderid_orders` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orseritem_bookid_book` FOREIGN KEY (`bookid`) REFERENCES `book` (`bookid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderitem`
--

LOCK TABLES `orderitem` WRITE;
/*!40000 ALTER TABLE `orderitem` DISABLE KEYS */;
INSERT INTO `orderitem` VALUES (1,5,1,2),(2,4,2,1),(3,1,3,3),(4,2,4,2),(5,11,4,1);
/*!40000 ALTER TABLE `orderitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `orderdate` datetime NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2017-04-25 16:22:58',1),(2,'2017-04-24 16:25:33',2),(3,'2017-04-19 16:25:47',3),(4,'2017-06-06 08:49:47',1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'997','tiamo','男',20),(2,'rlj','123456','男',20),(3,'jahentao','jahentao','男',21);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_book_quantities`
--

DROP TABLE IF EXISTS `v_book_quantities`;
/*!50001 DROP VIEW IF EXISTS `v_book_quantities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_book_quantities` AS SELECT 
 1 AS `quantities`,
 1 AS `bookid`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_booksaledetails`
--

DROP TABLE IF EXISTS `v_booksaledetails`;
/*!50001 DROP VIEW IF EXISTS `v_booksaledetails`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_booksaledetails` AS SELECT 
 1 AS `bookid`,
 1 AS `bookname`,
 1 AS `catalogid`,
 1 AS `catalogname`,
 1 AS `quantities`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'bookstore'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `e_updateHotsell` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = '' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`%`*/ /*!50106 EVENT `e_updateHotsell` ON SCHEDULE EVERY 1 MINUTE STARTS '2017-06-06 16:39:01' ON COMPLETION NOT PRESERVE ENABLE DO CALL p_updateHotsell() */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'bookstore'
--
/*!50003 DROP FUNCTION IF EXISTS `f_getCatalogSellAmount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `f_getCatalogSellAmount`(p_catalogid INT UNSIGNED) RETURNS int(11)
    READS SQL DATA
BEGIN
  DECLARE v_done int DEFAULT FALSE;
  DECLARE v_bid int;
  DECLARE v_q int;
  DECLARE v_sell_amount int;
  -- 查询catalogid类别下的bookid
  DECLARE bookid_cursor CURSOR FOR
  SELECT
    bookid
  FROM `book`
  WHERE `book`.catalogid = p_catalogid;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = TRUE;

  SET v_sell_amount = 0;
  -- 统计该类别下书籍的销量
  OPEN bookid_cursor;
read_loop:
  LOOP
    FETCH NEXT FROM bookid_cursor INTO v_bid;
    IF v_done THEN
      LEAVE read_loop;
    END IF;
    -- 嵌套查询
    SELECT
      quantities INTO v_q
    FROM (SELECT
        COUNT(quantity) quantities,
        bookid
      FROM `orderitem`
      GROUP BY bookid) AS t
    WHERE t.bookid = v_bid;
    SET v_sell_amount = v_sell_amount + v_q;
  END LOOP;
  CLOSE bookid_cursor;
  RETURN v_sell_amount;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_updateHotsell` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `p_updateHotsell`()
    COMMENT '更新热销商品表'
BEGIN
  -- 清除表中数据
  DELETE
    FROM `hotsell`
  WHERE 1 = 1;

  -- 把视图表里的数据按销量排序后，插入表中
  INSERT INTO hotsell (bookid, catalogid, salevolume)
    SELECT
      bookid,
      catalogid,
      quantities
    FROM v_booksaledetails
    ORDER BY quantities DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_book_quantities`
--

/*!50001 DROP VIEW IF EXISTS `v_book_quantities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_book_quantities` AS select count(`orderitem`.`quantity`) AS `quantities`,`orderitem`.`bookid` AS `bookid` from `orderitem` group by `orderitem`.`bookid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_booksaledetails`
--

/*!50001 DROP VIEW IF EXISTS `v_booksaledetails`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_booksaledetails` AS select distinct `b`.`bookid` AS `bookid`,`b`.`bookname` AS `bookname`,`c`.`catalogid` AS `catalogid`,`c`.`catalogname` AS `catalogname`,`d`.`quantities` AS `quantities` from ((`book` `b` join `catalog` `c`) join `v_book_quantities` `d`) where ((`b`.`catalogid` = `c`.`catalogid`) and (`d`.`bookid` = `b`.`bookid`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-07  9:21:12
