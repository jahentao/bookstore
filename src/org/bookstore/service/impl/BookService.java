package org.bookstore.service.impl;
import java.util.*;
import org.bookstore.model.*;
import org.bookstore.dao.*;
import org.bookstore.service.*;
public class BookService implements IBookService{
	protected IBookDAO bookDAO;
	public IBookDAO getBookDAO(){
		return bookDAO;
	}
	public void setBookDAO(IBookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}
	
	public List getNewBook(){
		return bookDAO.getNewBook();
	}
	public List getBookByCatalogidPaging(Integer catalogid,int currentPage,int pageSize){
		return bookDAO.getBookByCatalogidPaging(catalogid, currentPage, pageSize);
	}
	public int getTotalByCatalog(Integer catalogid){
		return bookDAO.getTotalByCatalog(catalogid);
	}
	public List getRequiredBookByName(String name) {
		return bookDAO.getRequiredBookByName(name);
	}
	public Book getBookById(Integer bookid){
		return bookDAO.getBookById(bookid);
	}
}
