package org.bookstore.service.impl;
import org.bookstore.dao.*;
import org.bookstore.model.*;
import org.bookstore.service.*;
public class UserService implements IUserService{
	private IUserDAO userDAO;
	public IUserDAO getUserDao(){
		return userDAO;
	}
	public void setUserDAO(IUserDAO userDAO){
		this.userDAO = userDAO;
	}
	
	public void saveUser(Users user){
		userDAO.saveUser(user);
	}

	public Users validateUser(String username, String password){
		return userDAO.validateUser(username, password);
	}

	public boolean existUser(String username){
		return userDAO.existUser(username);
	}
}
