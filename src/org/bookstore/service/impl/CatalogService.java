package org.bookstore.service.impl;
import java.util.*;

import org.bookstore.dao.*;
import org.bookstore.model.VBooksaledetails;
import org.bookstore.service.*;
public class CatalogService implements ICatalogService{
	private ICatalogDAO catalogDAO;
	public List getAllCatalogs(){
		return catalogDAO.getAllCatalogs();
	}
	
	public ICatalogDAO getCatalogDAO(){
		return catalogDAO;
	}
	public void setCatalogDAO(ICatalogDAO catalogDAO) {
		this.catalogDAO = catalogDAO;
	}

	@Override
	public List<VBooksaledetails> hotSellBooksRecommend(Integer catalogid) {
		return catalogDAO.getHotSellBooks(catalogid, 3);
	}
}
