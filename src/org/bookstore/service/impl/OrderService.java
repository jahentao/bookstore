package org.bookstore.service.impl;
import java.util.List;

import org.bookstore.dao.*;
import org.bookstore.service.*;
import org.bookstore.model.*;
public class OrderService implements IOrderService{
	private IOrderDAO orderDAO;
	public IOrderDAO getOrderDAO(){
		return orderDAO;
	}
	public void setOrderDAO(IOrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}
	
	public Orders saveOrder(Orders order) {
		return orderDAO.saveOrder(order);
	}
	@Override
	public List<Orders> findOrders(int userId) {
		return orderDAO.findOrders(userId);
	}
}
