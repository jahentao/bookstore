package org.bookstore.service;
import java.util.*;
import org.bookstore.model.*;
public interface IBookService {
	public List getNewBook();
	//分页查询
	public List getBookByCatalogidPaging(Integer catalogid,int currentPage,int pageSize);
	//得到该类别的图书的总数
	public int getTotalByCatalog(Integer catalogid);
	public List getRequiredBookByName(String name);
	public Book getBookById(Integer bookid);
}
