package org.bookstore.service;
import java.util.List;

import org.bookstore.model.VBooksaledetails;
public interface ICatalogService {
	public List getAllCatalogs();
	/**
	 * 推荐 catalogid 类别图书前3本热销书
	 * @param catalogid 书籍类别
	 * @return 热销书列表
	 */
	public List<VBooksaledetails> hotSellBooksRecommend(Integer catalogid);
}
