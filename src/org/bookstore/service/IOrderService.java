package org.bookstore.service;
import java.util.List;

import org.bookstore.model.*;
public interface IOrderService {
	public Orders saveOrder(Orders order);
	public List<Orders> findOrders(int userId);
}
