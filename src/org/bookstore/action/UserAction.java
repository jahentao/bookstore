package org.bookstore.action;
import java.util.*;
import org.bookstore.model.*;
import org.bookstore.service.*;
import com.opensymphony.xwork2.*;
public class UserAction extends ActionSupport{
	protected Users user;
	protected IUserService userService;
	public Users getUser() {
		return this.user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public IUserService getUserService(){
		return userService;
	}
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
	public String register() throws Exception {
		Users user1 = new Users();
		user1.setUsername(user.getUsername());
		user1.setPassword(user.getPassword());
		user1.setAge(user.getAge());
		user1.setSex(user.getSex());
		userService.saveUser(user1);
		return SUCCESS;
	}

	public String execute() throws Exception {
		Users u = userService.validateUser(user.getUsername(), user.getPassword());
		if (u != null) {
			Map session = ActionContext.getContext().getSession();
			session.put("user", u);
			return SUCCESS;
		}else {
			return ERROR;
		}
	}

	public String logout() throws Exception{
		//�û�ע��
		Map session=ActionContext.getContext().getSession();
		session.remove("user");
		session.remove("cart");
		return SUCCESS;
	}
}
