package org.bookstore.action;
import java.util.*;
import org.bookstore.util.*;
import org.bookstore.model.VBooksaledetails;
import org.bookstore.service.*;
import com.opensymphony.xwork2.*;
public class BookAction extends ActionSupport{
	protected ICatalogService catalogService;
	public ICatalogService getCatalogService(){
		return catalogService;
	}
	public void setCatalogService(ICatalogService catalogService) {
		this.catalogService = catalogService;
	}
	
	protected IBookService bookService;
	public IBookService getBookService(){
		return bookService;
	}
	public void setBookService(IBookService bookService) {
		this.bookService = bookService;
	}

	public String newBook() throws Exception{
		List books=bookService.getNewBook();
		Map request=(Map)ActionContext.getContext().get("request");
		request.put("books", books);
		return SUCCESS;
	}
	
	public String browseCatalog() throws Exception{
		List catalogs=catalogService.getAllCatalogs();
		Map request=(Map)ActionContext.getContext().get("request");
		request.put("catalogs",catalogs);
		return SUCCESS;
	}

	protected Integer catalogid;			//获得图书类别的ID
	private Integer currentPage=1;			//当前页
	//生成当前页的get和set方法
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	//生成图书ID的get和set方法
	public Integer getCatalogid() {
		return catalogid;
	}
	public void setCatalogid(Integer catalogid) {
		this.catalogid = catalogid;
	}
	//方法实现
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public String browseBookPaging() throws Exception{
		int totalSize=bookService.getTotalByCatalog(catalogid);
		// 热销书推荐
		List<VBooksaledetails> list = catalogService.hotSellBooksRecommend(catalogid);
		Pager pager=new Pager(currentPage,totalSize);
		List books=bookService.getBookByCatalogidPaging(catalogid, currentPage, pager.getPageSize());
		Map request=(Map)ActionContext.getContext().get("request");
		request.put("books", books);
		request.put("pager",pager);
		//购物车要返回继续购买时,需要记住返回的地址
		Map session=ActionContext.getContext().getSession();
		request.put("catalogid",catalogid);
		request.put("hotsellBooksDetails", list);
		return SUCCESS;
	}

	private String bookname;			//根据输入的书名或部分书名查询
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String searchBook() throws Exception {
		List books = bookService.getRequiredBookByName(this.getBookname());
		Map request = (Map)ActionContext.getContext().get("request");
		request.put("books",books);
		return SUCCESS;
	}
	
}
