package org.bookstore.action;

import java.util.List;
import java.util.Map;

import org.bookstore.model.Orders;
import org.bookstore.model.Users;
import org.bookstore.service.impl.OrderService;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class OrdersAction extends ActionSupport{
	OrderService orderService;
	public OrderService getOrderService() {
		return orderService;
	}
	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
	public String findOrders() throws Exception{
		Map session = ActionContext.getContext().getSession();
		if(session.get("user")==null){
			return ERROR;
		}
		else{
			Map request = (Map)ActionContext.getContext().get("request");
			Users user = (Users) session.get("user");
			List<Orders> orders = orderService.findOrders(user.getUserid());
			request.put("myorders", orders);
			return SUCCESS;
		}
	}
}
