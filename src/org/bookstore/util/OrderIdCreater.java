package org.bookstore.util;

import java.util.Date;
import java.util.Random;

public class OrderIdCreater {
	public static String createOrderId(int userId){
		Date date = new Date();
		long time = date.getTime();
		StringBuffer orderId = new StringBuffer(String.valueOf(userId));
		orderId.append(String.valueOf(time));
		int size = orderId.length();
		Random random = new Random();
		while(size<32){
			orderId.append(String.valueOf(random.nextInt(10)));
			size = orderId.length();
		}
		return orderId.toString();
	}
}
