package org.bookstore.dao;
import java.util.List;

import org.bookstore.model.VBooksaledetails;
public interface ICatalogDAO {
	public List getAllCatalogs();
	/**
	 * 获取 catalogid 类别图书前 limit 本热销书
	 * @param catalogid 书籍类别
	 * @param limit 默认为3
	 * @return 热销书列表
	 */
	public List<VBooksaledetails> getHotSellBooks(Integer catalogid, Integer limit);
}
