package org.bookstore.dao.impl;
import java.util.Iterator;
import java.util.List;

import org.bookstore.dao.*;
import org.bookstore.model.*;
import org.hibernate.*;
public class OrderDAO extends BaseDAO implements IOrderDAO{
	public Orders saveOrder(Orders order) {
		try{
			Session session=getSession();
			Transaction ts=session.beginTransaction();
			session.save(order);
			ts.commit();
			session.close();
			return order;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Orders> findOrders(int userId) {
		try{
			Session session=getSession();

			Query query=session.createQuery("from Orders o where o.user.userid=?");
			query.setParameter(0, 1);
			/*query.setFirstResult(startRow);
			query.setMaxResults(pageSize);*/
			List<Orders> orders = query.list();
			session.close();
			return orders;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
