package org.bookstore.dao.impl;
import java.util.*;
import org.bookstore.dao.*;
import org.bookstore.model.Book;
import org.bookstore.model.VBooksaledetails;
import org.hibernate.*;
public class CatalogDAO extends BaseDAO implements ICatalogDAO{
	public List getAllCatalogs(){
		try{
			Session session=getSession();
			Transaction ts=session.beginTransaction();
			Query createQuery = session.createQuery("from Catalog");
			List catalogs = createQuery.list();
			ts.commit();
			session.close();
			return catalogs;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public List<VBooksaledetails> getHotSellBooks(Integer catalogid, Integer limit) {
		try{
			Session session=getSession();
			Query query = session.createQuery("FROM VBooksaledetails v WHERE v.id.catalogid=? ORDER BY v.id.quantities DESC");
			query.setParameter(0, catalogid);
			query.setFirstResult(0);
			query.setMaxResults(limit);
			List<VBooksaledetails> catalogs = query.list();
			session.close();
			return catalogs;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
