package org.bookstore.dao;
import org.bookstore.model.*;
public interface IUserDAO {
	public void saveUser(Users user);
	public Users validateUser(String username, String password);
	public boolean existUser(String username);
}
