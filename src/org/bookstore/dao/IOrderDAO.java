package org.bookstore.dao;
import java.util.List;

import org.bookstore.model.*;
public interface IOrderDAO {
	public Orders saveOrder(Orders order);
	public List<Orders> findOrders(int userId);
}
