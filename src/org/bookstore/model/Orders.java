package org.bookstore.model;
import java.util.*;
//import java.sql.Timestamp;

/**
 * Orders entity. @author MyEclipse Persistence Tools
 */

public class Orders implements java.io.Serializable {

	// Fields

	private Integer orderid;
	private Users user;								//订单输入用户
	private Date orderdate;							//订单日期
	private Set orderitems = new HashSet();			//包含的订单项
	//private Timestamp orderdate;
	//private Integer userid;

	// Constructors

	/** default constructor */
	public Orders() {
	}

	/** full constructor */
	public Orders(Users user, Date orderdate) {
		this.user = user;
		this.orderdate = orderdate;
		//this.userid = userid;
	}

	// Property accessors

	public Integer getOrderid() {
		return this.orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public Users getUser() {
		return this.user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	
	public Date getOrderdate() {
		return this.orderdate;
	}

	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
/*
	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}
*/
	public Set getOrderitems() {
		return orderitems;
	}
	public void setOrderitems(Set orderitems) {
		this.orderitems = orderitems;
	}
}