# 项目说明

## 开发环境

- JDK 1.7
- Spring 3.2
- Hibernate 4
- Structs2
- Mysql 5.7
- Tomcat 8.5

### 项目运行

1、在MySQL中创建bookstore数据库，将sql/bookstore.sql 导入数据库中

然后依次导入 sql/separate/v_book_quantities.sql、sql/separate/v_bookSaleDetails.sql两个视图即可。

其余文件不用。sql/bookstore2.sql为MySQL 服务器端导出的脚本。

2、修改applicationContext中的数据库连接用户名和密码

如：

```xml
<bean id="dataSource"
<!-- other -->
	<property name="username" value="{your_db_username}"></property>
	<property name="password" value="{your_db_password}"></property>
</bean>
```

3、运行，在浏览器中输入http://localhost:8080/bookstore/

可以见到如下页面：

![bookstore_index](http://images-markdown.oss-cn-hangzhou.aliyuncs.com/course/WebService/bookstore_index.png)

### 实验要求：增加功能点

#### “我的订单”功能点

步骤分解：

a) 模型层：

经过设计和尝试，不需要改动原数据库


b) 视图层

- 页面上在用户登录后添加“我的订单”链接
- 编写“我的订单”展示页面
	- 页面展现 页面代码
	- 查询 逻辑代码
	- 分页显示 逻辑代码
	

c) 控制层

- 修改"结算"时的业务处理逻辑



#### "热销商品排行榜"功能点

步骤分解：

a) 模型层：

其实只要创建视图就行了，由于MySQL 5.6 视图不支持嵌套查询，需要分解，见sql/separate/v_book_quantities.sql、sql/separate/v_bookSaleDetails.sql两个视图。

b) 视图层

编写了WebRoot/hotsell.jsp文件，当点击进某类书籍后，显示该类“热销推荐”侧边栏。

c) 控制层

- 编写对视图的查询

  i) Hibernate 逆向工程，从视图生成实体类和映射文件：
  
```
   VBookQuantities.java、VBookQuantitiesId.java、VBooksaledetails.java、VBooksaledetailsId.java
   VBookQuantities.hbm.xml、VBooksaledetails.hbm.xml
```	

   由于视图没有主键，Hibernate采用的联合主键VBookQuantitiesId、VBooksaledetailsId
   
  ii) 修改已有接口及配置和实现
  
  添加Dao层添加`public List<VBooksaledetails> getHotSellBooks(Integer catalogid, Integer limit);`，Service层添加`public List<VBooksaledetails> hotSellBooksRecommend(Integer catalogid);` 默认推荐3本该类热销书籍。

	
- 页面数据填充

  动态填充页面数据，见WebRoot/hotsell.jsp。


##### 功能点实现效果图

![热销推荐](http://images-markdown.oss-cn-hangzhou.aliyuncs.com/course/WebService/%E5%8F%AE%E5%BD%93%E7%BD%91-%E7%83%AD%E9%94%80%E6%8E%A8%E8%8D%90-%E5%AE%9E%E7%8E%B0.png)

#### 其他

我觉得还可以添加的功能点：

- 权限管理
- 管理员 查看统计分析视图
- 引入支付接口

### 测试

#### 接口测试



#### 压力测试

- 并发量的上限

