package org.bookstore.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.bookstore.dao.impl.CatalogDAO;
import org.bookstore.model.VBooksaledetails;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ICatalogDAOTest {
	
	private ICatalogDAO dao;

	@Before
	public void setUp() throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		SessionFactory sf = (SessionFactory)context.getBean("sessionFactory");
		CatalogDAO catalogDAO = new CatalogDAO();
		catalogDAO.setSessionFactory(sf);
		dao = (ICatalogDAO)catalogDAO;
	}

	@Test
	public void testGetAllCatalogs() {
		List allCatalogs = dao.getAllCatalogs();
		assertTrue(allCatalogs.size()>0);
	}

	@Test
	public void testGetHotSellBooks() {
		List<VBooksaledetails> hotSellBooks = dao.getHotSellBooks(1, 3);
		assertTrue(hotSellBooks.size()>0);
	}

}
